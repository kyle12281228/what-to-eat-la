from django.db import models
from django.contrib.auth.models import User
import uuid

class OrderGroup(models.Model):
    code = models.CharField(default=uuid.uuid4().hex[:4], max_length=4, editable=False)
    manager = models.ManyToManyField(User)
    group_name = models.CharField(max_length=100)
    detail = models.TextField()

    def __str__(self):
        return self.group_name

class Menu(models.Model):
    name = models.CharField(max_length=100)
    detail = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class OrderConfigure(models.Model):
    name = models.CharField(max_length=100)
    order_group = models.ForeignKey(OrderGroup, on_delete=models.CASCADE)
    allow_edit = models.BooleanField(default=True)
    expire_time = models.DateTimeField()

    def __str__(self):
        return self.name

class Food(models.Model):
    name = models.CharField(max_length=100)
    price = models.IntegerField()
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    detail = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name} ({self.price})"

class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    detail = models.CharField(max_length=100)
    last_revise_time = models.DateTimeField(auto_now=True)
    is_paid = models.BooleanField(default=False)
    config = models.ForeignKey(OrderConfigure, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user} ordered {self.food} with details: {self.detail}"
