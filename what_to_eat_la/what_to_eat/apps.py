from django.apps import AppConfig


class WhatToEatConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "what_to_eat"
